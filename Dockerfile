FROM golang:alpine3.16
LABEL org.opencontainers.image.authors="evans.amoany@gmail.com"

WORKDIR	/srv
COPY newfile.go .
RUN go build -o newfile newfile.go
CMD ["/srv/newfile"]
